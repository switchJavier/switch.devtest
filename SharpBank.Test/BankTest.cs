﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void TestCustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new Savings()).OpenAccount(new Maxi_Saving()).OpenAccount(new Checking());
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary - John (3 accounts)", bank.CustomerSummary());
        }

        [Test]
        public void TestCheckingAccount()
        {
            int typeInterest = 0;
            Bank bank = new Bank();
            Account checkingAccount = new Checking();
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.00027397260273972606, bank.TotalInterestPaid(typeInterest), DOUBLE_DELTA);
        }

        [Test]
        public void TestSavingsAccount()
        {
            int typeInterest = 0;
            Bank bank = new Bank();
            Account savingsAccount = new Savings();
            bank.AddCustomer(new Customer("Bill").OpenAccount(savingsAccount));

            savingsAccount.Deposit(1500.0);

            Assert.AreEqual(0.00821917808219178, bank.TotalInterestPaid(typeInterest), DOUBLE_DELTA);
        }

        [Test]
        public void TestMaxiSavingsAccount()
        {
            int typeInterest = 0;
            Bank bank = new Bank();
            Account maxi_savingAccount = new Maxi_Saving();
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxi_savingAccount));

            maxi_savingAccount.Deposit(3000.0);

            Assert.AreEqual(0.410958904109589, bank.TotalInterestPaid(typeInterest), DOUBLE_DELTA);
        }

        [Test]
        public void TestTotalDailyInterestPaid()
        {
            int typeInterest = 0;
            Bank bank = new Bank();
            Account checkingAccount = new Checking();
            Account maxi_savingAccount = new Maxi_Saving();
            Account savingsAccount = new Savings();
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount).OpenAccount(maxi_savingAccount).OpenAccount(savingsAccount);
            bank.AddCustomer(bill);


            checkingAccount.Deposit(10000.0);
            savingsAccount.Deposit(15000.0);
            maxi_savingAccount.Deposit(500000.0);
            maxi_savingAccount.Withdraw(100.0);
            bank.TotalInterestPaid(typeInterest);

            Assert.AreEqual(1.4791780821917806, bank.TotalInterestPaid(typeInterest), DOUBLE_DELTA);
        }

        [Test]
        public void TestTotalYearlyInterestPaid()
        {
            int typeInterest = 1;
            Bank bank = new Bank();
            Account checkingAccount = new Checking();
            Account maxi_savingAccount = new Maxi_Saving();
            Account savingsAccount = new Savings();
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount).OpenAccount(maxi_savingAccount).OpenAccount(savingsAccount);
            bank.AddCustomer(bill);


            checkingAccount.Deposit(10000.0);
            savingsAccount.Deposit(15000.0);
            maxi_savingAccount.Deposit(500000.0);
            maxi_savingAccount.Withdraw(100.0);
            bank.TotalInterestPaid(typeInterest);

            Assert.AreEqual(539.90000000000009, bank.TotalInterestPaid(typeInterest), DOUBLE_DELTA);
        }

        [Test]
        public void TestTotalYearlyInterestPaid_Fail()
        {
            int typeInterest = 1;
            Bank bank = new Bank();
            Account checkingAccount = new Checking();
            Account maxi_savingAccount = new Maxi_Saving();
            Account savingsAccount = new Savings();
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount).OpenAccount(maxi_savingAccount).OpenAccount(savingsAccount);
            bank.AddCustomer(bill);

            maxi_savingAccount.Deposit(50000.0);
            maxi_savingAccount.Withdraw(100000.0);
            bank.TotalInterestPaid(typeInterest);

            Assert.AreEqual(539.90000000000009, bank.TotalInterestPaid(typeInterest), DOUBLE_DELTA);
        }
    }
}
