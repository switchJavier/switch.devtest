﻿using NUnit.Framework;
using System;
using System.Text;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = new Checking();
            Account savingsAccount = new Savings();
            Account maxi_SavingsAccount = new Maxi_Saving();

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount).OpenAccount(maxi_SavingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);
            savingsAccount.TransferToAcconut(200.0, maxi_SavingsAccount);

            Assert.AreEqual("Statement for Henry\r\n" +
                "Checking Account\r\n  deposit $100.00\r\n" +
                "Total $100.00\r\n\r\n" +
                "Savings Account\r\n  deposit $4,000.00\r\n  withdrawal $200.00\r\n  withdrawal $200.00\r\n" +
                "Total $3,600.00\r\n\r\n" +
                "Maxi_Saving Account\r\n  deposit $200.00\r\nTotal $200.00\r\n\r\n" +
                "Total In All Accounts $3,900.00\r\n", henry.GetStatement());

        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Savings());
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Savings());
            oscar.OpenAccount(new Checking());

            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar");
            oscar.OpenAccount(new Savings()).OpenAccount(new Checking()).OpenAccount(new Maxi_Saving());

            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }


        [Test]
        public void TestThreeAcounts_Fail()
        {
            Customer oscar = new Customer("Oscar");
            oscar.OpenAccount(new Savings()).OpenAccount(new Checking());

            Assert.AreEqual(3, oscar.GetNumberOfAccounts(), "Error with accounts numbers");

        }

        [Test]
        public void TestTransferToAcconut_Fail()
        {
            Account savingsAccount = new Savings();
            Account maxi_SavingsAccount = new Maxi_Saving();

            Customer henry = new Customer("Henry").OpenAccount(savingsAccount).OpenAccount(maxi_SavingsAccount);

            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);
            savingsAccount.TransferToAcconut(20000.0, maxi_SavingsAccount);

            Assert.AreEqual("Statement for Henry\r\n" +
                 "Savings Account\r\n  deposit $4,000.00\r\n  withdrawal $200.00\r\n  withdrawal $200.00\r\n" +
                 "Total $3,600.00\r\n\r\n" +
                 "Maxi_Saving Account\r\n  deposit $20000.00\r\nTotal $20000.00\r\n\r\n" +
                 "Total In All Accounts $3,800.00\r\n", henry.GetStatement());


        }
    }
}