﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void Transaction()
        {
            double ammount = 5;
            Transaction t = new Transaction(ammount);
            Assert.AreEqual(ammount, t.Amount);
        }
    }
}
