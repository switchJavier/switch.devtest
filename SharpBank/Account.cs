﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SharpBank
{
    public abstract class Account
    {

        public int AccountType { get; set; }
        public List<Transaction> transactions;

        public Account()
        {
            this.transactions = new List<Transaction>();
        }

        public Account(int accountType)
        {
            this.AccountType = accountType;
            this.transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
                throw new ArgumentException("amount must be greater than zero");
            transactions.Add(new Transaction(amount));
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
                throw new ArgumentException("amount must be greater than zero");
            else if (this.SumTransactions() < amount)
                throw new ArgumentException("The account dosen't have enough balance.");
            else
                transactions.Add(new Transaction(-amount));
        }

        public double SumTransactions()
        {
            return this.transactions.Sum(t => t.Amount);
        }

        public List<Transaction> GetTransaction()
        {
            return transactions;
        }

        public abstract string GetAccountType();

        public abstract double InterestEarned(int typeInterest);

        public string TransferToAcconut(double amount, Account depositAccount)
        {
            string resultado = string.Empty;
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    if (this.SumTransactions() >= amount)
                        this.Withdraw(amount);
                    else
                        throw new Exception("The account dosen't have enough balance!!!");

                    if (depositAccount != null)
                        depositAccount.Deposit(amount);
                    else
                        throw new Exception("The account dosen't exist!!!");


                    tran.Complete();
                    resultado = "Successfully!!!";
                }
                catch (Exception ex)
                {
                    tran.Dispose();
                    throw new Exception(ex.Message);
                }
            }
            return resultado;
        }

        public String StatementForAccount()
        {
            StringBuilder s = new StringBuilder();
            s.AppendLine(this.GetAccountType());
            double total = 0.0;
            foreach (Transaction t in this.transactions)
            {
                s.AppendLine("  " + (t.Amount < 0 ? "withdrawal" : "deposit") + " " + t.Amount.ToDollars());
                total += t.Amount;
            }
            s.AppendLine("Total " + total.ToDollars());
            return s.ToString();
        }

        public enum TypeInterest
        {
            DAILY = 0,
            YEARLY = 1
        }

    }
}
