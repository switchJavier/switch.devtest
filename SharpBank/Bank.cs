﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> customers;

        public Bank()
        {
            customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
        }

        public String CustomerSummary()
        {
            StringBuilder summary = new StringBuilder();
            summary.Append("Customer Summary");
            foreach (Customer c in customers)
                summary.AppendFormat(" - " + c.GetName() + " (" + c.GetNumberOfAccounts().WithWord("account") + ")");
            return summary.ToString();
        }

        public double TotalInterestPaid(int typeInterest)
        {
            double total = 0;
            foreach (Customer c in customers)
                total += c.TotalInterestEarned(typeInterest);
            return total;
        }

        public String GetFirstCustomer()
        {
            try
            {
                return customers[0].GetName();
            }
            catch (Exception e)
            {
                throw new Exception("Error while get Customers:  " + e.Message);
            }
        }
    }
}
