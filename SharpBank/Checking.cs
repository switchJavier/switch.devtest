﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Checking :  Account
    {
        //Checking accounts have a flat rate of 0.1%
        //Interest rates should accrue daily(incl.weekends), rates above are per-annum

        private const double RATE_RANGE = 0.001;
        private const int ANNUAL_PERIOD = 365;

        public Checking() : base()
        {

        }      

        public override string GetAccountType()
        {
            return "Checking Account";
        }

        override public double InterestEarned(int typeInterest)
        {          
            double interestEarned = 0;             

            if((int)TypeInterest.YEARLY==typeInterest)
                interestEarned = CalculatedInterest(typeInterest);
            else if((int)TypeInterest.DAILY==typeInterest)
                interestEarned = CalculatedInterest(typeInterest) / ANNUAL_PERIOD;
            else
                throw new Exception("Type of interest dosen't exist");

            return interestEarned;
        }

        private double CalculatedInterest(int typeInterest)
        {
            double interestEarned = 0;
            double amount = SumTransactions();

            interestEarned = amount * RATE_RANGE;

            return interestEarned;
        }
    }
}
