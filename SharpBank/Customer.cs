﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Customer
    {
        public String Name { get; set; }
        public List<Account> accounts;

        public Customer()
        {

        }

        public Customer(String name)
        {
            this.Name = name;
            this.accounts = new List<Account>();
        }

        public String GetName()
        {
            return Name;
        }

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned(int typeInterest)
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned(typeInterest);
            return total;
        }

        public String GetStatement()
        {
            StringBuilder statement = new StringBuilder();
            statement.AppendLine("Statement for " + Name);
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement.AppendLine(a.StatementForAccount());
                total += a.SumTransactions();

            }
            statement.AppendLine("Total In All Accounts " + total.ToDollars());
            return statement.ToString();
        }



    }
}
