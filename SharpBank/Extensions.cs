﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public static class Extensions
    {
        public static string WithWord(this int number, String word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        public static String ToDollars(this double d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }
    }
}
