﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
   public class Maxi_Saving : Account
    {
        //Change Maxi-Savings accounts to have an interest rate of 5% 
        //assuming no withdrawals in the past 10 days otherwise 0.1%

        private const double RATE_RANGE = 0.05;
        private const double RATE_RANGE_AFTER_WITHDRAW = 0.001;
        private const int ANNUAL_PERIOD = 365;
        private const int DAYS_AFTER_WITHDRAW = 10;
       
        public Maxi_Saving() : base()
        {

        }

        public override string GetAccountType()
        {
            return "Maxi_Saving Account";
        }

        override public double InterestEarned(int typeInterest)
        {
            double interestEarned = 0;

            if ((int)TypeInterest.YEARLY == typeInterest)
                interestEarned = CalculatedInterest(typeInterest);
            else if ((int)TypeInterest.DAILY == typeInterest)
                interestEarned = CalculatedInterest(typeInterest) / ANNUAL_PERIOD;
            else
                throw new Exception("Type of interest dosen't exist");

            return interestEarned;
        }

        private double CalculatedInterest(int typeInterest)
        {
            double interestEarned = 0;
            double amount = SumTransactions();
            List<Transaction> withdrawAccount = GetTransaction().Where(t => t.Amount < 0 && t.TransactionDate >= DateProvider.GetInstance().Now().AddDays(-DAYS_AFTER_WITHDRAW)).ToList();

            if (withdrawAccount != null && withdrawAccount.Count > 0)
                interestEarned = amount * RATE_RANGE_AFTER_WITHDRAW;
            else
                interestEarned = amount * RATE_RANGE;

            return interestEarned;
        }
    }
}
