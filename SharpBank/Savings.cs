﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Savings : Account
    {
        // Savings accounts have a rate of 0.1% for the first $1,000 then 0.2%
        private const double FIRST_RATE_RANGE = 0.001;
        private const double SECOND_RATE_RANGE = 0.002;
        private const int ANNUAL_PERIOD = 365;
        public Savings() : base()
        {

        }

        public override string GetAccountType()
        {
            return "Savings Account";
        }

        override public double InterestEarned(int typeInterest)
        {
            double interestEarned = 0;

            if ((int)TypeInterest.YEARLY== typeInterest)
                interestEarned = CalculatedInterest(typeInterest);
            else if((int)TypeInterest.DAILY == typeInterest)
                interestEarned = CalculatedInterest(typeInterest) / ANNUAL_PERIOD;
            else
                throw new Exception("Type of interest dosen't exist");

            return interestEarned;
        }

        private double CalculatedInterest(int typeIneterest)
        {
            double interestEarned = 0;

            double amount = SumTransactions();
            if (amount <= 1000)
                interestEarned = amount * FIRST_RATE_RANGE;
            else
                interestEarned = amount * SECOND_RATE_RANGE;

            return interestEarned;
        }
    }
}
